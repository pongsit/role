<?php
	
require_once("../system/init.php");

$role = new \pongsit\role\role();

if(!($_SESSION['user']['id']==1 || $role->check('admin'))){
	$view = new \pongsit\view\view('locked');
	echo $view->create();
	exit();
}

$role_infos = $role->get_all(array('order_by'=>'power'));
// Array ( 	[0] => Array ( [id] => 1 [name] => admin [power] => 100 [name_show] => ผู้บริหาร ) 
// 			[1] => Array ( [id] => 2 [name] => manager [power] => 90 [name_show] => ผู้จัดการ ) 
// 			[2] => Array ( [id] => 3 [name] => staff [power] => 80 [name_show] => เจ้าหน้าที่ ) 
//			[3] => Array ( [id] => 4 [name] => user [power] => 70 [name_show] => ผู้ใช้ระบบ ) ) 
// print_r($role_infos);
// exit();
if(empty($role_infos)){
	$roles = array();
	$roles[0] = array('id'=>1, 'name'=>'admin', 'power' => 100, 'name_show' => 'ผู้บริหาร');
	$roles[1] = array('id'=>2, 'name'=>'manager', 'power' => 90, 'name_show' => 'ผู้จัดการ');
	$roles[2] = array('id'=>3, 'name'=>'staff', 'power' => 80, 'name_show' => 'เจ้าหน้าที่');
	$roles[3] = array('id'=>4, 'name'=>'user', 'power' => 70, 'name_show' => 'ผู้ใช้ระบบ');

	foreach($roles as $k=>$vs){
		$role->insert($vs);
	}

	$role_infos = $role->get_all(array('order_by'=>'power'));
}

$list = '';
$limit = 10;
if(!empty($role_infos)){
	foreach($role_infos as $k=>$vs){
		$vs['card_name'] = ucfirst($vs['name']);
		if(!empty($vs['name_show'])){
			$vs['card_name'] = $vs['name_show'];
		}
		$role_id = $vs['id'];
		$role_user_alls = $role->user_get_all(array('role_id'=>$role_id,'limit'=>$limit));
// 		Array ( [0] => Array ( [user_id] => 1 ) ) 
// 		print_r($role_user_alls);
// 		exit();
		$_list = '';
		foreach($role_user_alls as $_k=>$_vs){
			$_list .= $view->block('list', array('id'=>$_vs['user_id']));
		}
// 		if(!(count($role_user_alls) < $limit)){
			$_list .= '
<div class="d-inline">
	<div class="text-center d-inline-block mr-2 mb-2" style="width:30px; height:50px;">
		<a href="'.$path_to_core.'user/list.php?role_id='.$role_id.'">
			<img class="w-100" src="'.$path_to_core.'system/img/icon/more.png">
		</a>
	</div>
</div>';
// 		}
		
		
		$vs['list'] = $_list;
		$vs['active_show']='';
		if($vs['active']!=1){$vs['active_show']='สถานะ: <span class="text-danger">ระงับการใช้งาน</span>';}
		$list .= $view->block('card',$vs);
	}
}

$variables['list'] = $list;

$variables['page-name'] = 'ตั้งค่าบทบาท';
echo $view->create($variables);
