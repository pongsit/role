CREATE TABLE `role` (
  `id` INT AUTO_INCREMENT primary key NOT NULL,
  `name` text NOT NULL,
  `power` int(11) NOT NULL DEFAULT '50',
  `name_show` text
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS role_user (
	user_id int(11) NOT NULL, 
	role_id int(11) NOT NULL
);

INSERT INTO `role` (`id`, `name`,`power`,`name_show`) VALUES (1, 'admin',100,'ผู้บริหาร');
INSERT INTO `role` (`id`, `name`,`power`,`name_show`) VALUES (2, 'manager',90,'ผู้จัดการ');
INSERT INTO `role` (`id`, `name`,`power`,`name_show`) VALUES (3, 'staff',80,'เจ้าหน้าที่');
INSERT INTO `role` (`id`, `name`,`power`,`name_show`) VALUES (4, 'user',70,'ผู้ใช้ระบบ');
INSERT INTO role_user (user_id,role_id) VALUES (1,1);INSERT INTO role_user (user_id,role_id) VALUES (1,1);