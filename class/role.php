<?php
namespace pongsit\role;


class role extends \pongsit\model\model{
	
	public function __construct() 
    {
    	parent::__construct();
    }
    
    // ---- this table
    
    function get_id($role_name){
		$query = "SELECT id FROM ".$this->table." WHERE name='$role_name';";
		$results = $this->db->query_array0($query);
		if(empty($results)){
			return false;
		}else{
			return $results['id'];
		}
		
	}
	
	function get_name($role_id){
		$query = "SELECT name FROM ".$this->table." WHERE id='$role_id';";
		$results = $this->db->query_array($query);
		if(empty($results[0]['name'])){
			return false;
		}else{
			return $results[0]['name'];
		}
	}
	function get_name_show($role_id){
		$query = "SELECT name_show FROM ".$this->table." WHERE id='$role_id';";
		$results = $this->db->query_array($query);
		if(empty($results[0]['name_show'])){
			return '';
		}else{
			return $results[0]['name_show'];
		}
	}
	function get_name_show_from_name($name){
		$role_id = $this->get_id($name);
		$name_show = $this->get_name_show($role_id);
		return $name_show;
	}
	function get_power($role_id){
		$query = "SELECT power FROM ".$this->table." WHERE id='$role_id' and role.active = 1 ;";
		$results = $this->db->query_array($query);
		if(empty($results[0]['power'])){
			return 0;
		}else{
			return $results[0]['power'];
		}
	}
	
	// ---- join
    
	function check($role_name){
		if(empty($_SESSION['user']['id'])){
			return false;
		}
		$query = 'SELECT count(role.id) as c 
					    FROM role 
				  INNER JOIN role_user ON role_user.role_id = role.id 
					   WHERE user_id='.$_SESSION['user']['id'].' and role.active = 1 
					     AND role.name="'.$role_name.'" 
					;';
		$roles = $this->db->query_array0($query);
		if(empty($roles['c'])){
			return false;
		}
		if($roles['c']>0){
			return true;
		}else{
			return false;
		}
	}
	function show(){
		$query = 'SELECT role.name
					FROM role_user 
			  INNER JOIN role ON role_user.role_id = role.id 
				   WHERE role_user.user_id='.$_SESSION['user']['id'].'
				;';
		$roles = $this->db->query_array($query);
		return $roles;	
	}

	function get_all_name_for($user_id){
		$query = 'SELECT role.name as role_name
					FROM role_user
			  INNER JOIN role ON role_user.role_id = role.id 
			  	   WHERE role_user.user_id='.$user_id.' and role.active = 1 
			  	ORDER BY role_user.role_id ASC
			  ;';
		$roles = $this->db->query_array($query);
		$outputs=array();
		foreach($roles as $key=>$values){
			$outputs[] = $values['role_name'];
		}
		return $outputs;	
	}
	function get_all_role_info_for($user_id){
		$query = 'SELECT *, role.name as name, role.id as id
					FROM role_user
			  INNER JOIN role ON role_user.role_id = role.id 
			  	   WHERE role_user.user_id='.$user_id.' and role.active = 1 
			  	ORDER BY role_user.role_id ASC
			  ;';
		$roles = $this->db->query_array($query);
		$outputs=array();
		foreach($roles as $key=>$values){
			$outputs[] = $values;
		}
		return $outputs;	
	}
	function get_all_name_show_for($user_id){
		$query = 'SELECT role.name_show as role_name_show
					FROM role_user
			  INNER JOIN role ON role_user.role_id = role.id 
			  	   WHERE role_user.user_id='.$user_id.' and role.active = 1 
			  	ORDER BY role_user.role_id ASC
			  ;';
		$roles = $this->db->query_array($query);
		$outputs=array();
		foreach($roles as $key=>$values){
			$outputs[] = $values['role_name_show'];
		}
		return $outputs;	
	}
	function get_all_id_for($user_id){
		$query = 'SELECT role_id
					FROM role_user
			  	   WHERE user_id='.$user_id.' 
			  	ORDER BY role_id ASC
			  	;';
		$roles = $this->db->query_array($query);
		$outputs=array();
		foreach($roles as $key=>$values){
			$outputs[] = $values['role_id'];
		}
		return $outputs;	
	}
	function get_all_role_id(){
		$all_ids = $this->get_all();
		$outputs = array();
		foreach($all_ids as $key=>$values){
			$outputs[] = $values['id'];
		}
		return $outputs;
	}
	function get_max_power($user_id){
		if($user_id==1){
			return 1000;
		}else{
			$query = 'SELECT max(role.power) as the_power 
						FROM role 
				  INNER JOIN role_user
						  ON role.id=role_user.role_id and role.active = 1 
					   WHERE user_id='.$user_id.'
					;';
			$infos = $this->db->query_array0($query);
			return $infos['the_power'];
		}
	}
	function get_all_above($user_id){
		$max_power = $this->get_max_power($user_id);
		$query = 'SELECT *  
					FROM role 
			  	   WHERE power > '.$max_power.'
			  	ORDER BY power DESC
			  	;';
		$infos = $this->db->query_array($query);
		return $infos;
	}
	function user_get_all($inputs){
		$role_id='';
		if(!empty($inputs['role_id'])){ 
			$role_id=$inputs['role_id']; 
		}else{
			return false;
		}
		$order_by='user.id';
		if(!empty($inputs['order_by'])){ $order_by=$inputs['order_by']; }
		$sort='DESC';
		if(!empty($inputs['sort'])){ $sort=$inputs['sort']; }
		$limit='';
		if(!empty($inputs['limit'])){ $limit='LIMIT '.$inputs['limit']; }
		$offset='';
		if(!empty($inputs['offset'])){ $offset='OFFSET '.$inputs['offset']; }
		$query = "SELECT user_id FROM role_user inner join user on user.id=role_user.user_id WHERE role_id='$role_id' and user.active = 1 ORDER BY $order_by $sort $limit $offset;";
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
}