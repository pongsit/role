<?php
	
require_once("../system/init.php");

$role = new \pongsit\role\role();
$user = new \pongsit\user\user();
$site = new \pongsit\site\site();

if(!($_SESSION['user']['id']==1 || $role->check('admin'))){
	$view = new \pongsit\view\view('locked');
	echo $view->create();
	exit();
}

if(empty(+$_GET['id'])){
	$view = new \pongsit\view\view('warning');
	echo $view->create();
	exit();
}
$id = $_GET['id'];

$role_infos = $role->get_info($id);
// Array ( [id] => 1 [name] => admin [power] => 100 [name_show] => ผู้บริหาร ) 
// print_r($role_infos);
// exit();

$info_show = '';
if(!empty($role_infos)){
	foreach($role_infos as $k=>$v){
		$blocks=array();
		$key_show='';
		if($k=='id'){continue;}
		switch($k){
			case 'name': $key_show='ชื่อ:'; break;
			case 'power': $key_show='Power:'; break;
			case 'name_show': $key_show='ชื่อแสดง:'; break;
			case 'active': 
				$key_show='สถานะ:'; 
				$blocks['label']=$key_show;
				$active = '';
				$inactive = '';
				if($v=='1'){$active='checked';}else{$inactive='checked';}
				$blocks['info']='<input type="radio" name="active" value="1" '.$active.'> ใช้งาน
								 <input type="radio" name="active" value="0" '.$inactive.'> ไม่ใช้งาน';
				break;
		}
		if(empty($key_show)){ continue; }
		
		if($k != 'active'){
			if($k=='name' && $v=='admin'){
				$blocks['label']=$key_show;
				$blocks['info']='<span class="mr-1">'.$v.'</span>';
			}else{
				$blocks['label']=$key_show;
				$blocks['info']='<input name="'.$k.'" class="form-control text-right" type="text" value="'.$v.'">';
			}
		}
		
		$info_show .= $view->block('form',$blocks);
	}
}

$variables['id'] = $id;
$variables['notification']='';
$variables['page-name'] = 'แก้บทบาท';

if(!empty($_POST)){
// 	Array ( [power] => 100 [name_show] => ผู้บริหาร [id] => 1 ) 
// 	print_r($_POST);
// 	exit();
	foreach($_POST as $key=>$value){
		if(!is_numeric($value)){
			if(empty($value)){unset($_POST[$key]);}
		}
	}
	if($_POST['power']>100){ $_POST['power']=100; }
	$role->update($_POST);
	header('Location:'.$path_to_core.'role/index.php');
}
$variables['info-list']=$info_show;
echo $view->create($variables);
