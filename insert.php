<?php
	
require_once("../system/init.php");

$role = new \pongsit\role\role();
$user = new \pongsit\user\user();
$site = new \pongsit\site\site();

if(!($_SESSION['user']['id']==1 || $role->check('admin'))){
	$view = new \pongsit\view\view('locked');
	echo $view->create();
	exit();
}

$role_infos = array('name'=>'','name_show'=>'','power'=>'');
foreach($role_infos as $k=>$v){
	$key_show='';
	if($k=='id'){continue;}
	switch($k){
		case 'name': $key_show='ชื่อ:'; break;
		case 'power': $key_show='Power:'; break;
		case 'name_show': $key_show='ชื่อแสดง:'; break;
	}
	if(empty($key_show)){ continue; }
	$blocks=array();
	
	if($k=='name' && $v=='admin'){
		$blocks['label']=$key_show;
		$blocks['info']='<span class="mr-1">'.$v.'</span>';
	}else{
		$blocks['label']=$key_show;
		$blocks['info']='<input name="'.$k.'" class="form-control text-right" type="text" value="'.$v.'">';
	}
	$info_show .= $view->block('form',$blocks);
}

$variables['id'] = $id;
$variables['notification']='';
$variables['page-name'] = 'แก้บทบาท';

if(!empty($_POST)){
// 	Array ( [power] => 100 [name_show] => ผู้บริหาร [id] => 1 ) 
// 	print_r($_POST);
// 	exit();
	foreach($_POST as $key=>$value){
		if(empty($value)){unset($_POST[$key]);}
	}
	if($_POST['power']>100){ $_POST['power']=100; }
	$role->insert($_POST);
	header('Location:'.$path_to_core.'role/index.php');
}
$variables['info-list']=$info_show;
echo $view->create($variables);
